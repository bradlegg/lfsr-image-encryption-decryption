/**********************************************************************
 *  Linear Feedback Shift Register (LFSR)
 **********************************************************************/

Author: Bradley Legg
OS: Linux (Ubuntu)
Machine: Dell XPS-13
Text editor: Emacs

The project was completed while studying Computer Science at The University of Massachusetts Lowell.
A detailed description of the project can be read on my LinkedIn.

Dependencies:
Though the project is designed to run on Linux, but will work on Windows as well if the files are moved to an IDE.
The SFML C++ library is required to run the project.
The SFML library can be installed with: sudo apt-get install libsfml-dev
Make sure you already have compiler (GCC) and make. These allow for use of the Makefile to run the program.

Running the program:
1. make
2. ./PhotoMagic input-file.png output-file.png (seed: random binary string length > 5) (tap position)
3. See that the output is saved as non-random noise in the project directory.
4. ./PhotoMagic output-file.png input-file.png (seed from part 2) (tap position from part 2)
Now if the output-file.png is saved in the directory as the original input-file.png from part 2, the execution was successful. 

Example run:
make
./PhotoMagic input-file.png output-file.png 01101000010100010000 16
./PhotoMagic output-file.png input-file.png 01101000010100010000 16

To reset, run "make clean" & delete output-file.png (or simply overwrite output-file.png with another execution)

I suggest a length of 5 or more to encrypt the image enough that the original image is no longer visible. The tap position must be less than or equal to seed length.

The encode and decode images are added in the project directory as an example of the program working correctly. 

The example image used is of fair use.
input-file.png source: https://commons.wikimedia.org/wiki/File:%E3%82%BF%E3%82%AA.png
